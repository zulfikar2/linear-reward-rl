#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "env.h"

double alpha = 0.15;
double beta = 0.15;

void LR1(double *p, int action, int *env);
void LRP(double *p, int action, int *env);

int main() {
	//p(k) array initalized to be (100/k)/100 each. (uniformly distributed)
	double *p = malloc(k * sizeof(double));
	for(int x = 0; x < k; x++) {
		p[x] = (100.0 / k) / 100.0;
	}
	
	int *env = initEnvironment();
	double probability = 0;
	double culmProbability = 0;
	int curr_action = -1;
	int optimal = 0;
	int totalReward = 0;
	//LRP
	printf("Running Linear Reward-Penalty algorithm\n\n");
	for(int a = 1; a <= RUNS; a++) {
		//rand [0,1]
		probability = (double)rand() / (double)RAND_MAX;
		culmProbability = 0;
		//iterate until culmulative probability >= random probability
		for(int i = 0; i < k; i++) {
			culmProbability += p[i];
			if(culmProbability >= probability) {
				curr_action = i;
				break;
			}
		}
		LRP(p, curr_action, env);
		
		totalReward += env[curr_action];
		if(optimalAction(env[curr_action]) == 1) {
			optimal++;
		}
		if(a % 100 == 0) {
			printf("Optimal actions done up to action %d : %d\n", a, optimal);
		}
	}
	printf("Average reward collected over 1000 actions : %d\n", totalReward / RUNS);
	
	//new array p (uniformly distributed)
	double *p1 = malloc(k * sizeof(double));
	for(int x = 0; x < k; x++) {
		p1[x] = (100.0 / k) / 100.0;
	}
	
	optimal = 0;
	curr_action = -1;
	totalReward = 0;
	//LR1
	printf("\nRunning Linear Reward-Inaction algorithm\n\n");
	for(int a = 1; a <= RUNS;a ++) {
		//rand [0,1]
		probability = (double)rand() / (double)RAND_MAX;
		culmProbability = 0;
		//iterate until culmulative probability >= random probability
		for(int i = 0; i < k; i++) {
			culmProbability += p1[i];
			if(culmProbability >= probability) {
				curr_action = i;
				break;
			}
		}
		LR1(p1, curr_action, env);
		totalReward += env[curr_action];
		if(optimalAction(env[curr_action]) == 1) {
			optimal++;
		}
		if(a % 100 == 0) {
			printf("Optimal actions done up to action %d : %d\n", a, optimal);
		}
	}
	printf("Average reward collected over 1000 actions : %d\n", totalReward / RUNS);
	return 0;
}

void LR1(double *p, int action, int *env) {
	int i = action;
	
	//signal is 1 (success)
	if(optimalAction(env[i]) == 1) {
		//pt+1(i) = pt(i) + α · (1 − pt(i)) 
		//increase slightly probability of choosing ai
		p[i] = p[i] + alpha * (double)(1.0 - p[i]);
		//for all j != i decrease probabilities proportionally
		for(int j = 0; j < k; j++) {
			if(i != j) {
				//pt+1(j) = (1 − α) · pt(j)
				p[j] = (double)(1.0 - alpha) * p[j];
			}
		}
	}
	//signal is 0 (failure)
	else {
		//inaction since beta = 0
	}
}

void LRP(double *p, int action, int *env) {
	int i = action;
	
	//signal is 1 (success)
	if(optimalAction(env[i]) == 1) {
		//pt+1(i) = pt(i) + α · (1 − pt(i)) 
		//increase slightly probability of choosing ai
		p[i] = p[i] + alpha * (double)(1 - p[i]);
		//for all j != i decrease probabilities proportionally
		for(int j = 0; j < k; j++) {
			if(i != j) {
				//pt+1(j) = (1 − α) · pt(j)
				p[j] = (double)(1.0 - alpha) * p[j];
			}
		}
	}
	//signal is 0 (failure)
	else {
		//for all j != i increase probabilities
		for(int j = 0; j < k; j++) {
			if(i != j) {
				//pt+1(j) = β/k−1 + (1 − β) · pt(j),
				//decrease this probability to keep the total sum at 1
				p[j] = beta/(double)(k-1) + (double)(1.0-beta) * p[j];
			}
		}
		//pt+1(i) = (1 − β) · pt(i)
		p[i] = (1 - beta) * p[i];
	}
}