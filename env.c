#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "env.h"

/*this environment can be
represented as a set of 10 different probabilities 
{q1, q2, q3, q4, q5, q6, q7, q8, q9, q10}, where qi is the probability of
getting 1 after doing the action ai 
(consequently, 1 − qi is the probability of getting 0). 
In your programs, you have to choose randomly values 
qk, k E {1, . . . , 10} that define each of the available actions.*/
int max = -1;

int *initEnvironment() {
	int* q = malloc(k * sizeof(double));
	srand((unsigned)time(NULL));
	
	for(int i = 0; i < k; i++) {
		//value 0-100
		q[i] = rand() % 100;
		if(q[i] > max)
			max = q[i];
	}
	
	return q;
}

int optimalAction(int qK) {
	if(qK == max)
		return 1;
	else
		return 0;
}