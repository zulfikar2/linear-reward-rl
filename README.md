# Linear-Reward RL

  a program that implements the following two learning automata: L R−I and L R−P .
These two types of learning automata are known as linear reward-inaction and linear reward-penalty algorithms,
respectively.

![](Explaination.png)