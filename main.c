#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "env.h"

#define epsilon 0.125

int maxA(int*);

/*Write a program that implements an o-greedy algorithm 
described in Section 2.3 of the 2nd edition of the textbook (2017). 
Your program must do at least 1000 rounds of interaction with an environment. 
Afterevery 100 rounds, print how many times an optimal action was chosen 
and an average reward collected over time.*/

int main() {
	int optimal = 0;
	//index of action
	int curr_action = -1;
	//sum of rewards for each action i
	int *reward = malloc(k * sizeof(double));
	int *env = initEnvironment();
	//mean of rewards : sum of reward of action i / num of times action i selected
	double *Q = malloc(k * sizeof(double));
	//number of times action i taken
	int *N = malloc(k * sizeof(double));
	//total reward
	int totalReward = 0;
	
	srand((unsigned)time(NULL));
	
	printf("Running Epsilon greedy algorithm\n\n");
	for(int a = 1; a <= RUNS; a++) {
		double probability = (double)rand() / (double)RAND_MAX;
		
		if(probability > epsilon){
			//arg max q(a) (probability 1 - epsilon)
			curr_action = maxA(reward);
		}
		else { 
			//rand action by agent (probability epsilon)
			curr_action = rand() % 10; //[0,9]
		}
		//R = bandit(A)
		reward[curr_action] = env[curr_action];
		totalReward += reward[curr_action];
		//N(A) = N(A) + 1
		N[curr_action] = N[curr_action] + 1;
		//Q(A) = Q(A) + 1/N(A) * [R - Q(A)]
		Q[curr_action] = Q[curr_action] + 1.0/(double)N[curr_action] * (reward[curr_action] - Q[curr_action]);
		
		if(optimalAction(env[curr_action]) == 1) {
			optimal++;
		}
		if(a % 100 == 0) {
			printf("Optimal actions done up to action %d : %d\n", a, optimal);
		}
	}

	printf("Average reward collected over 1000 actions : %d\n", totalReward / RUNS);
	return 0;
}

int maxA(int *R) {
	int max = -1;
	int index = -1;
	for(int i = 0; i < k; i++) {
		if(R[i] > max) {
			max = R[i];
			index = i;
		}
	}
	return index;
}