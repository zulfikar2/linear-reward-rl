To run use the following commands :

1 ) gcc -std=c99 main.c env.c -o main
./main

2) gcc -std=c99 main2.c env.c -o main2
./main2

main.c is the first question's program (greedy algorithm)
main2.c is the second question's algorithm (linear reward/penalty/inaction)